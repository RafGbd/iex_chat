defmodule ChatQueue do
  use GenServer
  ##### # External API
  def start_link do
    GenServer.start_link(__MODULE__, :empty, name: __MODULE__)
  end
  
  def get_stranger(user) do
    GenServer.call __MODULE__, user
  end

  def pop do
    GenServer.call __MODULE__, :pop
  end

  def status do
    GenServer.call __MODULE__, :status
  end

  
  ##### # GenServer implementation
  def handle_call(:status, _from, state) do
    IO.inspect state
    { :reply, {:ok}, state}
  end
  def handle_call(:pop, _from, user) do
    { :reply, {:ok}, :empty}
  end

  def handle_call(user, _from, :empty) do
    { :reply, {:wait}, user }
  end

  def handle_call(user, _from, stranger) do
    send(stranger["pid"], {:start, user}) 
    { :reply, {:start, stranger}, :empty}
  end
end