defmodule Stats do
    def start_link do
        Agent.start_link(fn -> 0 end, name: __MODULE__)
    end

    def add_user do
        Agent.update(__MODULE__, &(&1+1)) 
    end

    def del_user do
       Agent.update(__MODULE__, &(&1-1)) 
    end

    def total_users do
        Agent.get(__MODULE__, &(&1)) 
    end
end