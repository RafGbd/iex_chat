defmodule IexChat do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false


    dispatch = :cowboy_router.compile([

      # :_ causes a match on all hostnames.  So, in this example we are treating
      # all hostnames the same. You'll probably only be accessing this
      # example with localhost:8080.
      { :_, 
        
        # The following list specifies all the routes for hosts matching the
        # previous specification.  The list takes the form of tuples, each one 
        # being { PathMatch, Handler, Options}
        [
          {"/ws", WebsocketHandler, []}
      ]}
    ])
    { :ok, _ } = :cowboy.start_http(:http, 
                                    100,
                                   [{:port, 8080}],  
                                   [{ :env, [{:dispatch, dispatch}]}]
                                   ) 


    children = [
      # Define workers and child supervisors to be supervised
      worker(ChatQueue, []),
      worker(Stats, []),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: IexChat.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
