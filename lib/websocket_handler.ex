defmodule WebsocketHandler do
  @behaviour :cowboy_websocket_handler


  # We are using the websocket handler.  See the documentation here:
  #     http://ninenines.eu/docs/en/cowboy/HEAD/manual/websocket_handler/
  #
  # All cowboy HTTP handlers require an init() function, identifies which
  # type of handler this is and returns an initial state (if the handler
  # maintains state).  In a websocket handler, you return a 
  # 3-tuple with :upgrade as shown below.  This is essentially following
  # the specification of websocket, in which a plain HTTP request is made
  # first, which requests an upgrade to the websocket protocol.
  def init({tcp, http}, _req, _opts) do
    {:upgrade, :protocol, :cowboy_websocket}
  end

  # This is the first required callback that's specific to websocket
  # handlers.  Here I'm returning :ok, and no state since we don't 
  # plan to track ant state.
  #
  # Useful to know: a new process will be spawned for each connection
  # to the websocket.
  # :init - connection established
  #
  #
  #
  def websocket_init(_TransportName, req, _opts) do
    # Here I'm starting a standard erlang timer that will send
    # an empty message [] to this process in one second. If your handler
    # can handle more that one kind of message that wouldn't be empty.
    # :erlang.start_timer(1000, self(), [])
    Stats.add_user
    {:ok, req, {:init, %{} } }
  end

  # Required callback.  Put any essential clean-up here.

  def websocket_terminate(_reason, _req, state={:chatting, stranger}) do
    send(stranger["pid"], :stop)
    Stats.del_user
    :ok
  end

  def websocket_terminate(_reason, _req, state={:wait, %{}}) do
    ChatQueue.pop()
    Stats.del_user
    :ok
  end

  def websocket_terminate(_reason, _req, _state) do
    Stats.del_user
    :ok
  end

  # websocket_handle deals with messages coming in over the websocket.
  # it should return a 4-tuple starting with either :ok (to do nothing)
  # or :reply (to send a message back).

  def websocket_message_handle({:chatting, stranger}, msg=%{"command" => "MatchMeWithStranger"}, req) do
    send(stranger["pid"], :stop)
    websocket_message_handle({:init, %{}}, msg, req)
  end

  def websocket_message_handle({:init, %{}}, msg=%{"command" => "MatchMeWithStranger"}, req) do
    case ChatQueue.get_stranger(%{"pid"=> self()}) do
      {:wait} ->
        {:ok, reply} = JSX.encode(%{"command" => "wait"})
        {:reply, {:text, reply}, req, {:wait, %{}} }
      {:start, stranger} ->
        {:ok, reply} = JSX.encode(%{"command" => "startChat"})
        {:reply, {:text, reply}, req, {:chatting, stranger} }
    end
  end

  def websocket_message_handle({:chatting, stranger}, msg=%{"command" => "SendText"}, req) do
    send(stranger["pid"], {:send_text, msg["message"]})
    {:ok, req, {:chatting, stranger}}
  end

  def websocket_message_handle(state, message, req) do
    {:ok, req, state}
  end

  def websocket_handle({:text, content}, req, state) do
    { :ok, req_data } = JSX.decode(content)
    websocket_message_handle(state, req_data, req)
  end
  
  # Fallback clause for websocket_handle.  If the previous one does not match
  # this one just returns :ok without taking any action.  A proper app should
  # probably intelligently handle unexpected messages.
  def websocket_handle(_data, req, state) do    
    {:ok, req, state}
  end

  def websocket_info({:start, stranger}, req, {:wait, %{} }) do
    {:ok, reply} = JSX.encode(%{"command" => "startChat"})
    { :reply, {:text, reply}, req, {:chatting, stranger}}
  end

  def websocket_info({:send_text, msg}, req, {:chatting, stranger}) do
    {:ok, reply} = JSX.encode(%{"command" => "SendText", "message" => msg})
    { :reply, {:text, reply}, req, {:chatting, stranger}}
  end

  def websocket_info(:stop, req, {:chatting, stranger}) do
     {:ok, reply} = JSX.encode(%{"command" => "stopChat", "message" => ""})
     { :reply, {:text, reply}, req, {:init, %{}}}
  end
  # fallback message handler 
  def websocket_info(info, req, state) do
    IO.puts "websocket_info"
    IO.inspect info
    IO.inspect req
    IO.inspect state
    {:ok, req, state}
  end

end

